<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/kiryanaworld/admin/');
define('HTTP_CATALOG', 'http://localhost/kiryanaworld/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/kiryanaworld/admin/');
define('HTTPS_CATALOG', 'http://localhost/kiryanaworld/');

// DIR
define('DIR_APPLICATION', '/var/www/kiryanaworld/admin/');
define('DIR_SYSTEM', '/var/www/kiryanaworld/system/');
define('DIR_LANGUAGE', '/var/www/kiryanaworld/admin/language/');
define('DIR_TEMPLATE', '/var/www/kiryanaworld/admin/view/template/');
define('DIR_CONFIG', '/var/www/kiryanaworld/system/config/');
define('DIR_IMAGE', '/var/www/kiryanaworld/image/');
define('DIR_CACHE', '/var/www/kiryanaworld/system/storage/cache/');
define('DIR_DOWNLOAD', '/var/www/kiryanaworld/system/storage/download/');
define('DIR_LOGS', '/var/www/kiryanaworld/system/storage/logs/');
define('DIR_MODIFICATION', '/var/www/kiryanaworld/system/storage/modification/');
define('DIR_UPLOAD', '/var/www/kiryanaworld/system/storage/upload/');
define('DIR_CATALOG', '/var/www/kiryanaworld/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'ost');
define('DB_DATABASE', 'kiryanaworld');
define('DB_PORT', '3306');
define('DB_PREFIX', 'kr_');
