<?php
// Heading
$_['heading_title']    = 'Special Discount';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified Discount Products module!';
$_['text_edit']        = 'Edit Latest Module';

// Entry
$_['entry_name']       = 'Module Name';
$_['entry_limit']      = 'Limit';
$_['entry_width']      = 'Width';
$_['entry_height']     = 'Height';
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify discountproducts module!';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';
$_['error_width']      = 'Width required!';
$_['error_height']     = 'Height required!';
