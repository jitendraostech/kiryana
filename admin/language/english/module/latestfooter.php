<?php
// Heading
$_['heading_title']    = 'Latest Footer';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified latest footer module!';
$_['text_edit']        = 'Edit Latest footer Module';

// Entry
$_['entry_name']       = 'Module Name';
$_['entry_limit']      = 'Limit';
$_['entry_width']      = 'Width';
$_['entry_height']     = 'Height';
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify latest footer module!';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';
$_['error_width']      = 'Width required!';
$_['error_height']     = 'Height required!';
