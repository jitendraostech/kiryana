<?php
// Heading
$_['heading_title']    = 'Top Seller products';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified Top Seller products module!';
$_['text_edit']        = 'Edit Top Seller products Module';

// Entry
$_['entry_name']       = 'Module Name';
$_['entry_limit']      = 'Limit';
$_['entry_width']      = 'Width';
$_['entry_height']     = 'Height';
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Top Seller Products module!';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';
$_['error_width']      = 'Width required!';
$_['error_height']     = 'Height required!';
