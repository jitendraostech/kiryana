<?php

class ControllerModuleManufacturelist extends Controller{
	
	public function index($setting) {
		$this->load->language('module/manufacturelist');

		$data['heading_title'] = $this->language->get('heading_title');

		$this->load->model('catalog/manufacturer');


		$data['manufacturers'] = array();

		$manufacturers = $this->model_catalog_manufacturer->getManufacturers();
		if($manufacturers) {
			foreach($manufacturers as $manufacturer){
				$data['manufacturers'][] = array(
					'manufacturer_id'  => $manufacturer['manufacturer_id'],
					'name'        => $manufacturer['name'],
					'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $manufacturer['manufacturer_id'])	,
				);
			}
		
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/manufacturelist.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/manufacturelist.tpl', $data);
			} else {
				return $this->load->view('default/template/module/manufacturelist.tpl', $data);
			}
		}
	}
	
}

?>
