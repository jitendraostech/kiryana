<?php
class ModelModuleDiscountproducts extends Model {
	
	public function getDiscountProducts($limit) {
			$query = $this->db->query("SELECT distinct(product_id) FROM " . DB_PREFIX . "product_discount WHERE customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND quantity > 1 AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY quantity ASC, priority ASC, price ASC");

			return $query->rows;
		}

}
