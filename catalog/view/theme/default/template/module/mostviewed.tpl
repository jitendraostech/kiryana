<?php ?>
<div class="box box-product mostviewed">
<div class="box-heading">
	<span><?php echo $heading_title?></span>
</div>
<?php
//  $config = $this->registry->get('config'); 
// print_r($products);
?>
<div class="box-content">
<?php foreach($products as $product){?>
<?php $objlang = $this->registry->get('language');  $ourl = $this->registry->get('url');?>
<div class="product-block item-default" itemtype="http://schema.org/Product" itemscope>

	<?php if ($product['thumb']) {    ?>
		 <div class="image">
		 	<!-- text sale-->
			<!--<?php if( $product['special'] ) {   ?>
	    	<div class="product-label-special label"><?php echo $objlang->get( 'Sale' ); ?></div>
	    	<?php } ?>-->
			<a class="imgs" href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" class="img-responsive" /></a>
		</div>
	<?php } ?>
	
	<div class="product-meta">
		<div class="manufacturer"><?php echo $product['manufacturer']; ?></div>
		<h3 class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h3>
		
		<?php if( isset($product['description']) ){ ?> 
			<div class="description" itemprop="description"><?php echo utf8_substr( strip_tags($product['description']),0,220);?>...</div>
		<?php } ?>
			
		<?php if ($product['price']) { ?>
		<div class="price" itemtype="http://schema.org/Offer" itemscope itemprop="offers">
			<?php if (!$product['special']) {  ?>
				<span class="special-price"><?php echo $product['price']; ?></span>
				<?php if( preg_match( '#(\d+).?(\d+)#',  $product['price'], $p ) ) { ?> 
				<meta content="<?php echo $p[0]; ?>" itemprop="price">
				<?php } ?>
			<?php } else { ?>
				<!--<span class="price-old"><?php echo $product['price']; ?></span> -->
				<span class="price-new"><?php echo $product['special']; ?></span>
				<?php if( preg_match( '#(\d+).?(\d+)#',  $product['special'], $p ) ) { ?> 
				<meta content="<?php echo $p[0]; ?>" itemprop="price">
				<?php } ?>
			<?php } ?>
		</div>
		<?php } ?>
	</div>
</div>
<?php
}
  ?>
</div>
</div>
