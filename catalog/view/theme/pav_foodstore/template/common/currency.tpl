<?php if (count($currencies) > 1) { ?>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="currency">
    <div class="btn-group">
      <div class="">
          <?php foreach ($currencies as $currency) { ?>
            <?php if ($currency['code'] == $code) { ?>
            <?php if ($currency['symbol_left']) { ?>
            <a title="<?php echo $currency['title']; ?>" class="list-item currency-select">                                 
                <b class="item-symbol"><?php echo $currency['symbol_left']; ?></b>   
            </a>
            <?php } else { ?>
            <a title="<?php echo $currency['title']; ?>" class="list-item currency-select">
              <b class="item-symbol"><?php echo $currency['symbol_right']; ?></b>
            </a>
            <?php } ?>
            <?php } else { ?>
            <?php if ($currency['symbol_left']) { ?>
            <a href="javascript:void(0);" data-name="<?php echo $currency['code']; ?>" class="list-item currency-select">
              <?php echo $currency['symbol_left']; ?>
            </a>
            <?php } else { ?>
            <a href="javascript:void(0);" data-name="<?php echo $currency['code']; ?>" class="list-item currency-select"> 
              <?php echo $currency['symbol_right']; ?>
            </a>
            <?php } ?>
            <?php } ?>
          <?php } ?>
          <input type="hidden" name="code" value="" />
          <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
      </div>
    </div>      
  </form>
<?php } ?>
