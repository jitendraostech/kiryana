<div class="<?php echo str_replace('_','-',$blockid); ?> <?php echo $blockcls;?>" id="pavo-<?php echo str_replace('_','-',$blockid); ?>">
  <div class="container">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 box">
          <?php if( $content=$helper->getLangConfig('widget_custom_contact') ) {?>
            <?php echo $content; ?>
          <?php } ?>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 box column">
          <div class="box-heading"><span><?php echo $text_information; ?></span></div>
          <ul class="list-unstyled">
            <?php foreach ($informations as $information) { ?>
            <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
            <?php } ?>
          </ul>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 box column">
          <div class="box-heading"><span><?php echo $text_service; ?></span></div>
          <ul class="list-unstyled">
            <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
            <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
            <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
            <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
          </ul>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 box column">
          <div class="box-heading"><span><?php echo $text_extra; ?></span></div>
          <ul class="list-unstyled">
            <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
            <!--<li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
            <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>-->
            <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
          </ul>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 box column">
          <?php 
            $module = $helper->renderModule('pavnewsletter'); 
            if (count($module) && !empty($module)) { ?>
              <?php echo $module; ?>
          <?php } ?>
        </div>
    </div>
  </div>
</div>
