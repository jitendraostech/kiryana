<div class="box category">
  <div class="box-heading"><span><?php echo $heading_title; ?></span></div>

  <div class="box-content">
    <ul class="box-category list">
      <?php foreach ($categories as $category) { 
        $class = "";
        if(isset($category["children"]) && !empty($category["children"])){
          $class = "haschild";
        }
          $name = str_replace("(", '<span class="badge pull-right">',  $category['name'] );
          $category['name'] = str_replace(")", '</span>', $name); 
      ?>
      <li class="<?php echo $class; ?>">
      <?php if ($category['category_id'] == $category_id) { ?>
      <a href="<?php echo $category['href']; ?>" class="active"><?php echo $category['name']; ?></a>
      
        <?php if ($category['children']) { ?>
        <ul>  
          <?php foreach ($category['children'] as $child) { ?>
          <?php
            $child['name'] = str_replace("(", '<span class="badge pull-right">',  $child['name'] );
            $child['name'] = str_replace(")", '</span>', $child['name']);  
          ?>
          <li>
            <?php if ($child['category_id'] == $child_id) { ?>
            <a href="<?php echo $child['href']; ?>" class="active">&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a>
            <?php } else { ?>
            <a href="<?php echo $child['href']; ?>" class="">&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a>
            <?php } ?>
          </li>
          <?php } ?>
        </ul>
        <?php } ?>
      
      <?php } else { ?>
      <a href="<?php echo $category['href']; ?>" class=""><?php echo $category['name']; ?></a>
      <?php } ?>
      </li>
      <?php } ?>
    </ul>
  </div>

</div>