<?php $objlang = $this->registry->get('language'); ?>
<?php
	$themeConfig = (array)$this->config->get('themecontrol');
	$listingConfig = array(
		'category_pzoom'        => 1,
		'quickview'             => 0,
		'show_swap_image'       => 0,
		'product_layout'		=> 'default',
		'enable_paneltool'	    => 0
	);
	$listingConfig = array_merge($listingConfig, $themeConfig );
	$categoryPzoom = $listingConfig['category_pzoom'];
	$quickview     = $listingConfig['quickview'];
	$swapimg       = $listingConfig['show_swap_image'];
	$categoryPzoom = isset($themeConfig['category_pzoom']) ? $themeConfig['category_pzoom']:0; 

	$span = 12/$cols;

	$productLayout = DIR_TEMPLATE.$this->config->get('config_template').'/template/common/product/'.$listingConfig['product_layout'].'.tpl';	
	if( !is_file($productLayout) ){
		$listingConfig['product_layout'] = 'default';
	}
	$productLayout = DIR_TEMPLATE.$this->config->get('config_template').'/template/common/product/'.$listingConfig['product_layout'].'.tpl';	
	$button_cart = $objlang->get("button_cart");
?>
<div class="pavo-widget">
	<?php if( $show_title ) { ?>
	<div class="widget-heading menu-title"><?php echo $heading_title?></div>
	<?php } ?>
	<div class="widget-product-list <?php echo $addition_cls; ?>">
		<div class="widget-inner">
			<?php foreach ($products as $product) { ?>
			<div class="w-product clearfix col-lg-<?php echo $cols;?> col-md-<?php echo $cols;?> col-sm-12 col-xs-12">
				<?php if( isset($widget_name)){
	?>
	<div class="menu-title"><?php echo $widget_name;?></div>
	<?php
	}?>
	<div class="widget-product-list">
	  <div class="widget-inner">
	    
	      <?php foreach ($products as $product) { ?>
	      <div class="w-product clearfix">
	        <?php if ($product['thumb']) { ?>
	        <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
	        <?php } ?>
	        <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
	        <?php if ($product['price']) { ?>
	        <div class="price">
	          <?php if (!$product['special']) { ?>
	          <?php echo $product['price']; ?>
	          <?php } else { ?>
	          <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
	          <?php } ?>
	        </div>
	        <?php } ?>
	        <?php if ($product['rating']) { ?>
	        <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
	        <?php } ?>
	      </div>
	      <?php } ?>
	  </div>
	</div>  
			</div>
			<?php } ?>
		</div>
	</div>
</div>
