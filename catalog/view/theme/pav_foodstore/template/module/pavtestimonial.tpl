<?php if( count($testimonials) ) { ?>
<div class="box carousel slide pavtestimonial hidden-sm hidden-xs nopadding">
	<div class="box-content">
		<ul class="media-list">
		<?php foreach ($testimonials as $i => $testimonial) {  ?>
			<li class="media">
				<a class="pull-left" href="#">
		        	<img src="<?php echo $testimonial['thumb']; ?>" alt="">
		      	</a>
		      	<div class="media-body">
		      		<?php if(  $testimonial['description'] ) { ?>
					<div class="testimonial">
						<?php echo $testimonial['description']; ?>
					</div>
					<?php } ?>
					<?php if(  $testimonial['profile'] ) { ?>
					<div class="profile">
						<?php echo $testimonial['profile']; ?>
					</div>
					<?php } ?>
		      	</div>	
			</li>
		<?php } ?>
		</ul>
	</div>
</div>
<?php } ?>