<div class="blog-item">
	<?php if( $blog['thumb'] && $config->get('cat_show_image') )  { ?>
	<figure class="blog-body image">						
		<img src="<?php echo $blog['thumb'];?>" title="<?php echo $blog['title'];?>" alt="<?php echo $blog['title'];?>" class="img-responsive" />
	</figure>	
	<?php } ?>
	<div class="row">
		<div class="blog-meta col-lg-3 col-md-3 col-sm-4 col-xs-12">
			<ul>
				<?php if( $config->get('cat_show_created') ) { ?>
				<li class="created">
					<span class="fa fa-clock-o">   <?php echo $objlang->get("text_created");?> :</span>
					<span class="day"><?php echo date("d",strtotime($blog['created']));?></span>
					<span class="month"><?php echo date("M",strtotime($blog['created']));?></span> /
					<span class="month"><?php echo date("Y",strtotime($blog['created']));?></span>
				</li>
				<?php } ?>
				<?php if( $config->get('cat_show_author') ) { ?>
				<li class="author">
					<span><i class="fa fa-pencil"></i><?php echo $objlang->get("text_write_by");?></span> 
					<span class="t-color"><?php echo $blog['author'];?></span>
				</li>
				<?php } ?>
				<?php if( $config->get('cat_show_category') ) { ?>
				<li class="publishin">
					<span><i class="fa fa-thumb-tack"></i><?php echo $objlang->get("text_published_in");?></span>
					<a href="<?php echo $blog['category_link'];?>" class="t-color" title="<?php echo $blog['category_title'];?>"><?php echo $blog['category_title'];?></a>
				</li>
				<?php } ?>
				<?php if( $config->get('cat_show_hits') ) { ?>
				<li class="hits">
					<span><i class="fa fa-eye"></i><?php echo $objlang->get("text_hits");?></span>
					<span class="t-color"><?php echo $blog['hits'];?></span>
				</li>
				<?php } ?>
				<?php if( $config->get('cat_show_comment_counter') ) { ?>
				<li class="comment_count">
					<span><i class="fa fa-comment"></i><?php echo $objlang->get("text_comment_count");?></span>
					<span class="t-color"><?php echo $blog['comment_count'];?></span>
				</li>
				<?php } ?>
			</ul>
		</div>
		<div class="blog-body col-lg-9 col-md-9 col-sm-8 col-xs-12">
			<?php if( $config->get('cat_show_title') ) { ?>
			<div class="blog-header clearfix">
				<h3 class="blog-title"><a href="<?php echo $blog['link'];?>" title="<?php echo $blog['title'];?>"><?php echo $blog['title'];?></a></h3>
			</div>
			<?php } ?>
			<?php if( $config->get('cat_show_description') ) {?>			
			<section class="description">
				<?php echo $blog['description'];?>
			</section>
			<?php } ?>
			<?php if( $config->get('cat_show_readmore') ) { ?>
			<div class="blog-readmore">
				<a href="<?php echo $blog['link'];?>" class="button"><?php echo $objlang->get('text_readmore');?></a>
			</div>
			<?php } ?>
		</div>
	</div>
</div>